var webpack = require('webpack');
var nodemon = require('nodemon');
var path = require('path');
var _ = require('lodash');
var WebpackDevServer = require('webpack-dev-server');

var webpackBackConfig = require('./webpack.backend.config.js');

var nm;

var optimist = require('optimist');
require('webpack/bin/config-optimist')(optimist);

var argv = optimist.argv;
var cliOptions = require('webpack/bin/convert-argv')(optimist, argv);

var devServerConfing = {
    publicPath: '/assets',
    contentBase: './dist',
    historyApiFallback: true,
    inline: true,
    hot: true,
    stats: { colors: true },
    proxy: {
        '*': 'http://localhost:3000'
    }
};

new WebpackDevServer(webpack(cliOptions), devServerConfing).listen(80, 'localhost', function (err, result) {
    if (err) {
        console.log(err);
    }
    else {
        console.log('Webpack dev server listening at localhost:8080');
    }
});

var backendCompiler = webpack(webpackBackConfig);

backendCompiler.plugin('compile', function() {
    console.log('Building server...');
});

Error.stackTraceLimit = 30;
var lastHash = null;
function compilerCallback(err, stats) {
    if(err) {
        lastHash = null;
        console.error(err.stack || err);
        if(err.details) console.error(err.details);
        return;
    }
    if(stats.hash !== lastHash) {
        lastHash = stats.hash;
        process.stdout.write(stats.toString({
            colors: true
        }) + "\n");
    }
}

backendCompiler.plugin('done', function(res) {
    console.log('Restarting server...');
    if (!nm) {
        nm = nodemon({
            execMap: {
                js: 'node'
            },
            script: path.join(__dirname, 'dist/server.js'),
            ignore: ['*'],
            ext: 'noop'
        })
    } else {
        nm.restart()
    }
});

backendCompiler.watch(100, compilerCallback);
