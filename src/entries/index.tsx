/// <reference path='../defines.d.ts' />

import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import $ from 'jquery';

import { App } from '../components/app/app';
import { AppState, initialState } from '../state';
import { Provider, Store, applyMiddleware, createStore as createReduxStore } from '../redux';
import { historyPlugin } from '../lib/history';
import app from '../stores/app';

require('nyc-public/common.blocks/font/_roboto/font_roboto.css');
require('./index.styl');


function runApp(state: AppState) {
    ReactDOM.render(renderApp(state), document.getElementById('react-app'));
}

export function renderApp(state: AppState) {
    return <Provider store={ finalCreateStore(state) }>
        <App />
    </Provider>
}

const logger = store => next => action => {
    console.log('dispatching', action);
    let result = next(action);
    let state = store.getState();
    console.log('next state', state);
    return result;
};

function createStore(state: AppState): Store {
    return applyMiddleware(thunk, logger)(createReduxStore)(app, state || initialState);
}

function finalCreateStore(state: AppState): Store {
    let store = createStore(state);
    if (typeof window != 'undefined') {
        historyPlugin(store);
    }
    return store;
}

if (typeof window !== 'undefined') {
    window['runApp'] = runApp;
}
