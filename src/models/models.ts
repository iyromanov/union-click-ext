import moment from 'moment';
type Moment = moment.Moment;

export interface Currency {
    symbol: string,
    minorNumber: number
}

export interface Account {
    name: string,
    number: string,
    balance: number,
    currency: Currency
}

export interface Amount {
    value: number,
    currency: Currency
}

export enum RecipientType {
    Account = 'account' as any,
    Card = 'card' as any,
    PhoneNumber = 'phone' as any
}

export interface Recipient {
    account?: Account,
    name?: string,
    type?: RecipientType,
    userValues: { [key: string]: RecipientUserValue }
}

export interface RecipientUserValue {
    value: string,
    error?: string
}

export enum Status {
    OK = 'ok' as any,
    FAIL = 'fail' as any,
    IN_PROGRESS = 'repeat' as any
}

export interface Transaction {
    id: string;
    debitAccount: Account;
    recipient: Recipient;
    sum: Amount;
    fee: Amount;
    purpose: string;
    dateExecute: Moment;
    status: Status;
    authorizationCode: string;
}
