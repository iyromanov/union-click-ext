import { spread } from 'arui/src/lib/assign';
import moment from 'moment';

import { Transaction, Status } from './models';
import { TransferState } from '../state';
import { mocks } from '../mocks/mocks';
import { ExchangeService } from '../services/ExchangeService';


//TODO: remove mock values
export function transactionFromTransferState(state: TransferState): Transaction {
    return {
        id: '' + Math.random() * 1000,
        debitAccount: state.debitAccount,
        recipient: state.recipient,
        sum: state.debitAmount,
        fee: state.feeAmount,
        purpose: '',
        dateExecute: moment(),
        status: Math.random() > 0.3 ? Status.OK : Status.FAIL,
        authorizationCode: '4KUDPL5'
    };
}

export function transactionToTransferState(transaction: Transaction, state: TransferState): TransferState {
    //TODO: if account doesn't exist?
    let stateWithoutAmounts = spread(state, {
        recipient: transaction.recipient,
        debitAccount: state.debitAccounts.filter(account => {
            return account.number === transaction.debitAccount.number
        })[0]
    });
    return recalculateAmountsOnChangeDebitAmount(transaction.sum.value, stateWithoutAmounts);
}

export function recalculateAmountsOnChangeDebitAmount(amount: number, state: TransferState): TransferState {
    let exchangeRate = ExchangeService.getRate(state.debitAccount.currency, state.recipient.account.currency);
    return spread(state, {
        debitAmount: {
            value: amount,
            currency: state.debitAccount.currency
        },
        creditAmount: {
            value: (amount / state.debitAccount.currency.minorNumber) * exchangeRate,
            currency: state.recipient.account.currency
        },
        feeAmount: {
            value: amount * mocks.feeRate,
            currency: state.debitAccount.currency
        }
    })
}

export function recalculateAmountsOnChangeCreditAmount(amount: number, state: TransferState): TransferState {
    let exchangeRate = ExchangeService.getRate(state.recipient.account.currency, state.debitAccount.currency);
    return spread(state, {
        creditAmount: {
            value: amount,
            currency: state.recipient.account.currency
        },
        debitAmount: {
            value: (amount * exchangeRate) * state.debitAccount.currency.minorNumber,
            currency: state.debitAccount.currency
        },
        feeAmount: {
            value: ((amount * exchangeRate) * state.debitAccount.currency.minorNumber) * mocks.feeRate,
            currency: state.debitAccount.currency
        },
    });
}

export function sortByDate(transactions: Transaction[]): Transaction[] {
    transactions.sort((t1, t2) => {
        if (t1.dateExecute < t2.dateExecute) {
            return 1;
        } else if (t1.dateExecute > t2.dateExecute) {
            return -1;
        } else {
            return 0;
        }
    });
    return transactions;
}
