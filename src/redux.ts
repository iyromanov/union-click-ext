import {
    Action as ReduxAction,
    ActionFunction as ReduxActionFunction,
    createStore as reduxCreateStore,
    applyMiddleware as reduxApplyMiddleware,
    StoreCreator,
    Store as ReduxStore,
    Dispatch as ReduxDispatch
} from 'redux';

import {
    Provider,
    Connect,
    connect as reduxConnect,
    DispatchProps as ReduxDispatchProps
} from 'react-redux';

import { ActionType } from './actions';
import { AppState } from './state';
import * as actions from './actions';

export {
    Provider, actions
};

export type Action = ReduxAction<ActionType>;
export type ActionFunction = ReduxActionFunction<State, Action>;
export type State = AppState;
export type Store = ReduxStore<State, Action>;
export type Dispatch = ReduxDispatch<State, Action>;
export type ActionCreators = typeof actions;
export type DispatchProps = ReduxDispatchProps<State, Action>;

export let createStore: StoreCreator<State, Action> = reduxCreateStore;
export let connect: Connect<State, Action, Dispatch> = reduxConnect;
export let applyMiddleware = reduxApplyMiddleware
