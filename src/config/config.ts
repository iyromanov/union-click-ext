import { RecipientType } from '../models/models';

class AppConfigClass {
    menuHeader: MenuItemConfig[];
    menuFooter: MenuItemConfig[];
    switcherLinks: MenuItemConfig[];
    recipientSwitcherButtons: RecipientSwitcherButtonConfig[];
    validSmsCodeLength: number = 4;

    constructor() {
        this.menuHeader = [{
            title: 'Счета',
            link: '#'
        }, {
            title: 'Карты',
            link: '#'
        }, {
            title: 'Кредиты',
            link: '#'
        }, {
            title: 'Депозиты',
            link: '#'
        }, {
            title: 'Инвестиции',
            link: '#'
        }, {
            title: 'Переводы',
            link: '#'
        }];

        this.menuFooter = [{
            title: 'Сайт Альфа-Банка',
            link: '#'
        }, {
            title: 'Обратная связь',
            link: '#'
        }, {
            title: 'Помощь',
            link: '#'
        }];

        this.switcherLinks = [{
            title: 'Новый перевод',
            link: '/',
        }, {
            title: 'История переводов',
            link: '/history'
        }];

        this.recipientSwitcherButtons = [{
            type: RecipientType.Account,
            name: 'Счёт',
            pattern: '1111 1111 1111 1111 1111',
            hint: 'Введите номер счёта'
        },{
            type: RecipientType.Card,
            name: 'Карта',
            pattern: '1111 1111 1111 1111',
            hint: 'Введите номер карты'
        }, {
            type: RecipientType.PhoneNumber,
            name: 'Телефон',
            pattern: '11 111 11 11',
            hint: 'Введите номер телефона'
        }]
    }
}

export interface MenuItemConfig {
    title: string,
    link: string
}

export interface RecipientSwitcherButtonConfig {
    type: RecipientType,
    name: string,
    pattern: string,
    hint: string
}

export let config = new AppConfigClass();
