import { Action } from './redux';

import { Account, Transaction, RecipientType } from './models/models';
import { transactionFromTransferState } from './models/model-helper';
import { TransactionsFilter } from './state';

export enum ActionType {
    ChangeRecipientType = 'CHANGE_RECIPIENT_TYPE' as any,
    ChangeRecipientValue = 'CHANGE_RECIPIENT_VALUE' as any,
    ChangeRecipientName = 'CHANGE_RECIPIENT_NAME' as any,
    ChangeDebitAccount = 'CHANGE_DEBIT_ACCOUNT' as any,
    ChangeDebitAmount = 'CHANGE_DEBIT_AMOUNT' as any,
    ChangeCreditAmount = 'CHANGE_CREDIT_AMOUNT' as any,
    EnterSmsCode = 'ENTER_SMS_CODE' as any,
    ChangeLocation = 'CHANGE_LOCATION' as any,
    ValidateTransfer = 'VALIDATE_TRANSFER' as any,
    UnlockForm = 'UNLOCK_FORM' as any,
    ValidateSmsCode = 'VALIDATE_SMS_CODE' as any,
    ValidateSmsCodeResolve = 'VALIDATE_SMS_CODE_RESOLVE' as any,

    LoadTransferHistory = 'LOAD_TRANSFER_HISTORY' as any,
    ShowMoreTransactions = 'SHOW_MORE_TRANSACTIONS' as any,
    ChangeTransactionsFilter = 'CHANGE_TRANSACTIONS_FILTER' as any,
    RepeatTransaction = 'REPEAT_TRANSACTION' as any
}

export interface ChangeDebitAccount extends Action {
    accountNumber: string
}
export function changeDebitAccount(accountNumber: string) {
    return {
        type: ActionType.ChangeDebitAccount,
        accountNumber: accountNumber
    }
}

export interface ChangeRecipientType extends Action {
    recipientType: RecipientType
}
export function changeRecipientType(recipientType: RecipientType) {
    return {
        type: ActionType.ChangeRecipientType,
        recipientType: recipientType
    }
}

export interface ChangeRecipientValue extends Action {
    value: string
}
export function changeRecipientValue(value: string) {
    return {
        type: ActionType.ChangeRecipientValue,
        value: value
    }
}

export interface ChangeRecipientName extends Action {
    name: string
}
export function changeRecipientName(name: string) {
    return {
        type: ActionType.ChangeRecipientName,
        name: name
    }
}

export interface ChangeDebitAmount extends Action {
    value: number
}
export function changeDebitAmount(value: number) {
    return {
        type: ActionType.ChangeDebitAmount,
        value: value
    }
}


export interface ChangeCreditAmount extends Action {
    value: number
}
export function changeCreditAmount(value: number) {
    return {
        type: ActionType.ChangeCreditAmount,
        value: value
    }
}

export interface EnterSmsCode extends Action {
    value: number
}
export function enterSmsCode(value: string) {
    return (dispatch, getState) => {
        dispatch(validateSmsCode());
        setTimeout(() => {
            let transferState = getState().transfer;
            let transaction = transactionFromTransferState(transferState);
            dispatch(validateSmsCodeResolve(transaction));
        }, 1500);
    };
    // return {
    //     type: ActionType.EnterSmsCode,
    //     value: value
    // }
}

export interface ChangeLocation extends Action {
    value: string
}
export function changeLocation(location: string) {
    return {
        type: ActionType.ChangeLocation,
        value: location
    }
}

export function validateTransfer() {
    return {
        type: ActionType.ValidateTransfer
    }
}

export function validateSmsCode() {
    return {
        type: ActionType.ValidateSmsCode
    }
}

export interface ValidateSmsCodeResolve extends Action {
    transaction: Transaction
}
export function validateSmsCodeResolve(transaction: Transaction) {
    return {
        type: ActionType.ValidateSmsCodeResolve,
        transaction: transaction
    }
}

export function unlockForm() {
    return {
        type: ActionType.UnlockForm
    }
}


export interface LoadTransferHistory extends Action {
    transactions: Transaction[]
}
export function loadTransferHistory() {
    return {
        type: ActionType.LoadTransferHistory,
    }
}

export function showMoreTransactions() {
    return {
        type: ActionType.ShowMoreTransactions
    }
}

export interface RepeatTransaction extends Action {
    transaction: Transaction
}
export function repeatTransaction(transaction: Transaction) {
    return {
        type: ActionType.RepeatTransaction,
        transaction: transaction
    }
}

export interface ChangeTransactionsFilter extends Action {
    filter: TransactionsFilter
}
export function changeTransactionsFilter(filter: TransactionsFilter) {
    return {
        type: ActionType.ChangeTransactionsFilter,
        filter: filter
    }
}
