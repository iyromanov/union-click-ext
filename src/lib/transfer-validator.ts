import { spread } from 'arui/src/lib/assign';
import { cloneDeep } from 'arui/src/lib/cloneDeep';

import { TransferState } from '../state';
import { Recipient, RecipientType } from '../models/models';
import { mocks } from '../mocks/mocks';

export interface ValidationResult {
    ok: boolean,
    message?: string
}

const OK_RESULT: ValidationResult = {
    ok: true
}

export class TransferValidator {

    static validateRecipient(recipient: Recipient): ValidationResult {
        let value = !!recipient.userValues[recipient.type] ? recipient.userValues[recipient.type].value : '';
        return this.validateRecipientValue(value, recipient.type);
    }

    static validateRecipientValue(value: string, type: RecipientType): ValidationResult {
        let ok, message;
        switch (type) {
            case RecipientType.Card: {
                ok = value && value.length == 16;
                message = !ok ? 'Номер карты должен состоять из 16 цифр': undefined;
                break;
            }
            case RecipientType.PhoneNumber: {
                ok = value && value.length == 9;
                message = !ok ? 'Некорректное значение номера телефона. Введите номер в формате +375 XX XXX XX XX' : undefined;
                break;
            }
            default: {
                ok = value && value.length == 20;
                message = !ok ? 'Номер счета должен состоять из 20 цифр' : undefined;
                break;
            }
        }
        return {
            ok,
            message
        }
    }

    static validateRecipientName(name: string): ValidationResult {
        if (name && name.length > 0) {
            return OK_RESULT;
        } else {
            return {
                ok: false,
                message: 'Введите значение'
            }
        }
    }

    static validateDebitAmountFromState(state: TransferState) {
        return state.debitAmount ?
            this.validateDebitAmount(state, state.debitAmount.value) : OK_RESULT;
    }

    static forceValidateDebitAmountFromState(state: TransferState) {
        return this.validateDebitAmount(state, state.debitAmount ? state.debitAmount.value : undefined);
    }

    static validateDebitAmount(state: TransferState, value: number) {
        if (!value || value === 0) {
            return {
                ok: false,
                message: 'Введите сумму больше 0'
            }
        } else if (value <= state.debitAccount.balance) {
            return OK_RESULT;
        } else {
            return {
                ok: false,
                message: 'Недостаточно денег на счете'
            }
        }
    }

    static validateCreditAmount(state: TransferState, value: number) {
        if (value === 0) {
            return {
                ok: false,
                message: 'Введите сумму больше 0'
            }
        }
        if ((value / mocks.exchangeRate) * state.debitAccount.currency.minorNumber <= state.debitAccount.balance) {
            return OK_RESULT;
        } else {
            return {
                ok: false,
                message: 'Недостаточно денег на счете'
            }
        }
    }

    static validateTransferState(state: TransferState): TransferState {
        //TODO: implement it
        let out = cloneDeep(state);
        let recipientType = out.recipient.type;
        let recipientValidResult = this.validateRecipient(out.recipient);
        let debitAmountValidResult = this.forceValidateDebitAmountFromState(out);
        let recipientNameValidResult = OK_RESULT;

        out.recipient.userValues[out.recipient.type].error = recipientValidResult.message;

        if (recipientType == RecipientType.PhoneNumber) {
            recipientNameValidResult = this.validateRecipientName(out.recipient.name);
            out = spread(out, {
                recipientNameFieldError: recipientNameValidResult.message
            });
        }

        out = spread(out, {
            isRecipientValid: recipientValidResult.ok && recipientNameValidResult.ok
        });

        out = spread(out, {
            debitAmountFieldError: debitAmountValidResult.message,
            isDebitAmountValid: debitAmountValidResult.ok
        });

        return out;
    }

}
