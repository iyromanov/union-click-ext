import moment from 'moment';
import { formatters as formattersExt } from 'alfa-components/lib/formatters';

import { Account, Amount, Currency, Status } from '../models/models';

type Moment = moment.Moment;

function amountWithMinority(amount: Amount): number {
    // return (amount.value / amount.currency.minorNumber);
    // return parseFloat((amount.value / amount.currency.minorNumber).toFixed(2));
    return Math.round(amount.value / amount.currency.minorNumber * 100) / 100;

}

function amountToString(amount: Amount): string {
    if (!amount) {
        return '';
    }
    return formattersExt.formatAmount(amountWithMinority(amount));
}

function amountWithCurrencyToString(amount: Amount): string {
    if (!amount) {
        return '';
    }
    return amountToString(amount)  + ' ' + amount.currency.symbol;
}

function balanceToString(account: Account) {
    return formattersExt.formatAmount(account.balance / account.currency.minorNumber) + ' ' + account.currency.symbol;
}

function accountToString(account: Account): string {
    return account.number + ' ' + balanceToString(account);
}

function getMonth(date: moment.Moment): string {
    let month;
    switch (date.month()) {
        case 0: month = 'января'; break;
        case 1: month = 'февраля'; break;
        case 2: month = 'марта'; break;
        case 3: month = 'апреля'; break;
        case 4: month = 'мая'; break;
        case 5: month = 'июня'; break;
        case 6: month = 'июля'; break;
        case 7: month = 'августа'; break;
        case 8: month = 'сентября'; break;
        case 9: month = 'октября'; break;
        case 10: month = 'ноября'; break;
        case 11: month = 'декабря'; break;
        default: month = date.format('MMMMMM'); break;
    }

    return month;
}

function formatDate(date: Moment): string {
    return date.format('DD ') + getMonth(date);
}

function formatTime(date: Moment): string {
    return date.format('HH:mm');
}

function formatStatus(status: Status) {
    let stringStatus;
    switch(status) {
        case Status.OK: stringStatus = 'Перевод выполнен'; break;
        case Status.FAIL: stringStatus = 'Не выполнен'; break;
        case Status.IN_PROGRESS: stringStatus = 'На обработке'; break;
        default: stringStatus = 'А черт знает, чем все это закончилось'; break;
    }

    return stringStatus;
}

function formatAccountWithNameAndShrinking(account: Account) {
    //TODO: fix dangerous code
    return account.name + ' ' + maskAccountNumber(account);
}

function maskAccountNumber(account: Account) {
    let number = account.number;
    return '\u2022\u2022\u2022\u2022 ' + account.number.substring(number.length - 4, number.length);
}

export let formatters = {

    amountWithMinority: amountWithMinority,

    amountToString: amountToString,

    amountWithCurrencyToString: amountWithCurrencyToString,

    accountToString: accountToString,

    balanceToString: balanceToString,

    formatDate: formatDate,

    formatTime: formatTime,

    formatStatus: formatStatus,

    formatAccountWithNameAndShrinking: formatAccountWithNameAndShrinking,

    maskAccountNumber: maskAccountNumber

};
