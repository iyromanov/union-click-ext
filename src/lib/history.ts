// import { createHistory, createHashHistory } from 'history';
import { Store } from '../redux';
import * as actions from '../actions';

let createHistory = require('history').createHistory;
let createHashHistory = require('history').createHashHistory;

function createHistoryObject() {
    if (typeof window == 'undefined') {
        return;
    }

    let historyImpl = window.history && window.history.pushState
        ? createHistory
        : createHashHistory;

    let history = historyImpl();

    return history;
}

let history = createHistoryObject();

export function historyPlugin(store: Store) {
    if (typeof window === 'undefined') {
        return;
    }

    let historyImpl = window.history && window.history.pushState
        ? createHistory
        : createHashHistory;
    let history = historyImpl();

    let prevState = store.getState();
    store.subscribe(() => {
        let state = store.getState();
        if (prevState.transfer.location != state.transfer.location) {
            navigate(store.getState().transfer.location);
        }
        prevState = state;
    });

    history.listen(location => {
        store.dispatch(actions.changeLocation(location.pathname));
    })
}

function navigate(path: string): void {
    history.pushState({}, path);
}
