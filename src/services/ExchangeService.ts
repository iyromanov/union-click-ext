import { Amount, Currency } from '../models/models';
import { formatters } from '../lib/formatters';

import { mocks } from '../mocks/mocks';

export class ExchangeService {

    static getFormattedExchangeRate(baseCurrency: Currency, secondCurrency: Currency): string {
        return formatters.amountWithCurrencyToString({
            value: baseCurrency.minorNumber,
            currency: baseCurrency
        }) + ' - ' + formatters.amountWithCurrencyToString({
            value: this.getRate(baseCurrency, secondCurrency) * secondCurrency.minorNumber,
            currency: secondCurrency
        });
    }

    static getRate(baseCurrency: Currency, secondCurrency: Currency): number {
        if (this.isEqualCurrencies(baseCurrency, secondCurrency)) {
            return 1;
        } else if (this.isEqualCurrencies(baseCurrency, mocks.RUR_CURRENCY) &&
            this.isEqualCurrencies(secondCurrency, mocks.BY_CURRENCY)) {
            return 262;
        } else {
            return 1/262;
        }
    }

    private static isEqualCurrencies(first: Currency, second: Currency): boolean {
        return first.symbol === second.symbol;
    }

}
