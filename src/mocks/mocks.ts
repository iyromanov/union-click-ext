import moment from 'moment';

import { spread } from 'arui/src/lib/assign';

import { Account, Amount, Currency, Transaction, Recipient, RecipientType, Status } from '../models/models';

let RUR_CURRENCY = {
    symbol: '\u20BD',
    minorNumber: 100
} as Currency;

let BY_CURRENCY = {
    symbol: 'Br',
    minorNumber: 1
} as Currency;

let recipient: Recipient = {
    account: { name: 'Зарплатный счет', number: null, balance: 7593, currency: BY_CURRENCY },
    name: 'Шнайдер',
    userValues: {}
};

let DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

class MocksClass {
    RUR_CURRENCY = RUR_CURRENCY;
    BY_CURRENCY = BY_CURRENCY;
    debitAccounts: Account[] = [
        { name: 'Зарплатный счет', number: '40705982740874611001', balance: 123300, currency: RUR_CURRENCY },
        { name: 'Текущий счет', number: '40705987461001827401', balance: 23546, currency: RUR_CURRENCY },
        { name: 'Текущий счет', number: '40709478573674613409', balance: 434430, currency: RUR_CURRENCY },
        { name: 'Текущий счет', number: '407053434645434343432', balance: 499925, currency: RUR_CURRENCY },
        { name: 'Белор счет', number: '407053434645434343498', balance: 2332, currency: BY_CURRENCY }
    ];
    recipientAccount: Account = { name: 'Зарплатный счет', number: null, balance: 7593,
        currency: BY_CURRENCY
    };
    recipient: Recipient = recipient;
    reasons = [
        'Текущие расходы родственникам',
        'Другое назначение'
    ];
    costItems = [
        'Финансовые операции',
        'Семья и дети',
        'Автомобиль',
        'Кредит'
    ]
    firstAmount: Amount = { value: 100, currency: RUR_CURRENCY };
    secondAmount: Amount = { value: 262, currency: BY_CURRENCY };
    exchangeRate: number = 262;
    feeRate: number = 0.01;

    transactions: Transaction[] = [
        {
            id: "1234",
            debitAccount: {
                name: 'Текущий счет',
                number: '407053434645434343432',
                balance: 499925,
                currency: RUR_CURRENCY
            },
            recipient: spread(recipient, {
                type: RecipientType.PhoneNumber,
                userValues: {
                    phone: { value: '293230921'}
                }
            }),
            fee: {value: 262, currency: RUR_CURRENCY},
            sum: {value: 34262, currency: RUR_CURRENCY},
            purpose: "Семья и дети",
            dateExecute: moment("2015-09-15 10:12:00", DATE_FORMAT),
            status: Status.OK,
            authorizationCode: "4KFDPL5"
        },
        {
            id: "1235",
            debitAccount: {
                name: 'Текущий счет',
                number: '407053434645434343432',
                balance: 499925,
                currency: RUR_CURRENCY
            },
            recipient: spread(recipient, {
                type: RecipientType.Account,
                userValues: {
                    account: { value: '40101234323232323386'}
                }
            }),
            fee: {value: 0, currency: RUR_CURRENCY},
            sum: {value: 362, currency: RUR_CURRENCY},
            purpose: "Семья и дети",
            dateExecute: moment("2015-06-15 10:12:00", DATE_FORMAT),
            status: Status.FAIL,
            authorizationCode: "4KUDPL5"
        },
        {
            id: "1236",
            debitAccount: {
                name: 'Текущий счет',
                number: '407053434645434343432',
                balance: 499925,
                currency: RUR_CURRENCY
            },
            recipient: spread(recipient, {
                type: RecipientType.Card,
                userValues: {
                    card: { value: '4868976409653213'}
                }
            }),
            fee: {value: 0, currency: RUR_CURRENCY},
            sum: {value: 12000, currency: RUR_CURRENCY},
            purpose: "Семья и дети",
            dateExecute: moment("2015-08-15 01:12:00", DATE_FORMAT),
            status: Status.IN_PROGRESS,
            authorizationCode: "4KPAPL5"
        }

    ] as Transaction[];
}

export let mocks = new MocksClass();
