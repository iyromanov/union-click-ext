import { config } from './config';
import { App } from '../components/app/app';
import { renderApp } from '../entries/index';
import { initialState } from '../state';

let express = require('express'),
    path = require('path'),
    React = require('react'),
    ReactDOMServer = require('react-dom/server'),
    indexTemplate = require('./templates/index.handlebars'),
    hash = require('../../hash.json').hash;


let app = express();

app
    .get(['/', '/history'], (req, resp, next) => {
        resp.send(indexTemplate({
            content: ReactDOMServer.renderToString(renderApp(initialState)),
            initialState: JSON.stringify(initialState),
            hash: hash
        }));
    })
    .use('/assets', express.static(path.join(process.cwd(), 'dist', 'assets')))
    .listen(config.port);

console.log(path.join(process.cwd(), 'dist', 'assets'));
console.log('Sever started on port %s', config.port);
