import moment from 'moment';

import { Account, Amount, Transaction, Recipient, RecipientType, RecipientUserValue } from './models/models';

import { mocks } from './mocks/mocks';

type Moment = moment.Moment;

export interface AppState {
    transfer: TransferState,
    transfersHistory: TransfersHistoryState
}

export interface TransferState {
    location?: string,
    recipient: Recipient,
    recipientFieldError?: string,
    recipientNameFieldError?: string,
    debitAccount: Account,
    debitAccounts: Account[],
    debitAmount?: Amount,
    debitAmountFieldError?: string,
    creditAmount?: Amount,
    creditAmountFieldError?: string,
    feeAmount?: Amount,
    isRecipientValid: boolean,
    isDebitAccountValid: boolean,
    isDebitAmountValid: boolean,
    isValid: boolean,
    isReadOnly: boolean,
    isSmsCodeSending?: boolean
}

export interface TransfersHistoryState {
    filter: TransactionsFilter,
    lastTransactionMode?: boolean,
    transactions: Transaction[],
}

export interface TransactionsFilter {
    startDate?: Moment,
    endDate?: Moment,
    searchWord?: string
}

export let initialState: AppState = {
    transfer: {
        recipient: {
            type: RecipientType.Account,
            userValues: initUserValues(),
        },
        debitAccount: mocks.debitAccounts[0],
        debitAccounts: mocks.debitAccounts,
        isRecipientValid: false,
        isDebitAccountValid: true,
        isDebitAmountValid: false,
        isValid: false,
        isReadOnly: false
    },
    transfersHistory: {
        filter: {},
        transactions: []
    }
}

function initUserValues(): { [key: string]: RecipientUserValue } {
    let userValue = { value: '', error: undefined };
    return {
        account: userValue,
        card: userValue,
        phone: userValue
    }
}
