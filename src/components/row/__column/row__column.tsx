import React from 'react';
import * as theme from 'arui-nyc/src/theme';

const cn = theme.className('row')('column');

export enum RowColumnSize {
    Small = 's' as any,
    Medium = 'm' as any
}

export interface RowColumnProps extends React.DOMAttributes {
    size?: RowColumnSize
}

export class RowColumn extends React.Component<RowColumnProps, any> {

    static defaultProps = {
        size: RowColumnSize.Medium
    }

    render() {
        return <div className={ cn({ size: this.props.size }) }>{ this.props.children }</div>
    }

}
