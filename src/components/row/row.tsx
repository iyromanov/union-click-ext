import React from 'react';
import * as theme from 'arui-nyc/src/theme';

const cn = theme.className('row');

require('./row.styl');

export class Row extends React.Component<any, any> {

    render() {
        return <div className={ cn() } >{ this.props.children }</div>
    }

}
