import React from 'react';
import * as theme from 'arui-nyc/src/theme';

import { ButtonSize } from 'arui-nyc/src/button/button';
import {
    StringSelect,
    ButtonSelectProps,
    SelectWidth,
    SingleOption
} from 'arui-nyc/src/select/select';
import { formatters as formattersExt } from 'alfa-components/lib/formatters';

import { Account } from '../../../models/models';
import { formatters } from '../../../lib/formatters';

let { formatAccountWithShrinking, formatAmount } = formattersExt;
let { balanceToString, maskAccountNumber } = formatters;

require('./account-select.styl');

const cn = theme.className('account-select');

export interface AccountSelectProps extends React.CommonAttributes, theme.ThemeProps {
    accounts: Account[];
    selectedAccount?: Account;
    handleAccountSelect?: (option: SingleOption<string>) => void;
    disabled?: boolean
}

export interface AccountSelectState { }

export class AccountSelect extends React.Component<AccountSelectProps, AccountSelectState> {
    static contextTypes = theme.themeContext;

    render(): React.ReactElement<any> {
        return (
            <StringSelect
                options={ this.formatAccountsOptions() }
                width={ SelectWidth.Available }
                closeOnSelect={ true }
                onOptionClick={ this.handleAccountSelect }
                disabled={ this.props.disabled }
            />
        );
    }

    renderAccountTitle(account: Account): React.ReactElement<any> {
        return (
            <div className={ cn('title') }>
                <span className={ cn('name') }>
                    { account.name }
                </span>
                <span className={ cn('number') }>
                    { maskAccountNumber(account) }
                </span>
                <span className={ cn('amount') }>
                    { balanceToString(account) }
                </span>
            </div>
        );
    }

    formatAccountsOptions(): SingleOption<string>[] {
        return this.props.accounts.map(a => {
            return {
                title: this.renderAccountTitle(a),
                value: a.number,
                checked: this.props.selectedAccount && this.props.selectedAccount.number === a.number
            };
        });
    }

    handleAccountSelect = (option: SingleOption<string>) => {
        if (this.props.handleAccountSelect) {
            this.props.handleAccountSelect(option);
        }
    }
}
