import autobind from 'autobind-decorator';
import { debounce } from 'arui/src/lib/debounce';
import React from 'react';

import { connect } from '../../../redux';
import { KeyboardCode } from 'arui/src/lib/keyboard';
import {
    Autocomplete,
    TypedAutocompleteSelect,
    SingleOption,
    SelectSize,
    SelectWidth
} from 'arui-nyc/src/select/select';

import { Icon, IconSize } from 'arui-nyc/src/icon/icon';

const CHANGE_DEBOUNCE = 300;

let StringAutocomplete: TypedAutocompleteSelect<string> = Autocomplete as any;

export interface SearchBarState {
    value?: string;
    options?: SingleOption<string>[]
}

interface SearchBarStateProps {
    query?: string;
    placeholder?: string;
}

interface SearchBarProps extends React.CommonAttributes, SearchBarStateProps {
    onSearch: (query: string) => void;
}

export class SearchBar extends React.Component<SearchBarProps, SearchBarState> {
    constructor(props, context) {
        super(props, context);

        this.state = {
            value: this.props.query,
            options: []
        };

        this.notifySearchChangeDebounced = debounce(this.notifySearchChangeDebounced, CHANGE_DEBOUNCE);
    }

    componentWillReceiveProps(nextProps: SearchBarProps) {
        if (this.state.value !== nextProps.query) {
            this.setState({ value: nextProps.query })
        }
    }

    render() {
        return (
            <StringAutocomplete
                className={ this.props.className }
                options={ this.state.options }
                size={ SelectSize.Large }
                onKeyDown={ this.handleKeyDown }
                inputProps={ {
                    onChange: this.handleChange,
                    placeholder: this.props.placeholder,
                    value: this.state.value,
                    withClearButton: true,
                    leftAddons: [ <Icon size={ IconSize.Small } tool={ 'search' } /> ]
                } }
            />
        )
    }

    notifySearchChange() {
        this.props.onSearch(this.state.value);
    }

    @autobind
    notifySearchChangeDebounced() {
        this.notifySearchChange();
    }

    @autobind
    handleKeyDown(e: React.KeyboardEvent): void {
        if (e.which === KeyboardCode.Enter) {
            this.notifySearchChange();
        }
    }

    @autobind
    handleChange(value: string): void {
        this.setState({ value });
        this.notifySearchChangeDebounced();
    }
}
