import React from 'react';
import { FormField, FormFieldSize, FormFieldView } from 'arui-nyc/src/form-field/form-field'
import { Label, LabelSize } from 'arui-nyc/src/label/label'
import { Input, InputWidth, InputRuntime } from 'arui-nyc/src/input/input'
import { AmountInput } from 'arui-nyc/src/amount-input/amount-input'
import * as theme from 'arui-nyc/src/theme';

import { mocks } from '../../mocks/mocks'
import { Account, Amount } from '../../models/models';
import { formatters } from '../../lib/formatters';

require('./debit-amount-field.styl');

const cn = theme.className('debit-amount-field');
let { amountToString } = formatters;

export interface DebitAmountFieldProps {
    account?: Account,
    amount?: Amount,
    error?: string,
    onValueChange: (value: number, formatted: string) => void,
    focused?: boolean,
    disabled?: boolean
}

export class DebitAmountField extends React.Component<DebitAmountFieldProps, any> {

    refs: {
       [key: string]: React.Component<any, any>;
       amountInput: React.Component<any, any> & InputRuntime;
   }

   componentDidUpdate() {
       //TODO: not works
       this.props.focused && this.refs.amountInput.focus();
   }

    render() {
        return (
            <FormField
                className={ cn() }
                size={ FormFieldSize.Large }
                view={ FormFieldView.Compact }
                label={ <Label size={ LabelSize.Large }>Перевести</Label> }
            >
                <AmountInput
                    className={ cn('amount-input') }
                    width={ InputWidth.Available }
                    rightAddons={ [
                        <div className={ cn('currency') }>{ this.props.account && this.props.account.currency.symbol }</div> ]
                    }
                    value={ amountToString(this.props.amount) }
                    onValueChange={ this.handleAmountChange }
                    error={ this.props.error }
                    disabled={ this.props.disabled }
                    ref={ 'amountInput' }
                />
            </FormField>
        );
    }

    handleAmountChange = (value: number, formatted: string) => {
        this.props.onValueChange(value, formatted);
    }

}
