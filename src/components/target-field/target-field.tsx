import React from 'react';
import { FormField, FormFieldSize, FormFieldView } from 'arui-nyc/src/form-field/form-field';
import { Label, LabelSize } from 'arui-nyc/src/label/label';
import { Input, InputWidth } from 'arui-nyc/src/input/input';
import { StringSelect, SingleOption, SelectWidth } from 'arui-nyc/src/select/select';
import * as theme from 'arui-nyc/src/theme';

import { mocks } from '../../mocks/mocks';

const cn = theme.className('target-field');

export interface TargetFieldProps {
    disabled?: boolean
}

export interface TargetFieldState {
    reason: string,
}

export class TargetField extends React.Component<TargetFieldProps, TargetFieldState> {

    constructor(props) {
        super(props);
        this.state = {
            reason: mocks.reasons[0]
        }
    }

    render() {
        return (
            <FormField
                size={ FormFieldSize.Large }
                view={ FormFieldView.Compact }
            >
                <StringSelect
                    options={ this.formatOptions(mocks.reasons, this.state.reason) }
                    width={ SelectWidth.Available }
                    onOptionClick={ (option: SingleOption<string>): void => {
                        this.setState({ reason: option.value });
                    } }
                    disabled={ this.props.disabled }
                />
            </FormField>
        );
    }

    formatOptions(options: string[], value: string): SingleOption<string>[] {
        return options.map((option: string) => {
            return {
                value: option,
                checked: option == value
            }
        });
    }

}
