import React from 'react';
import moment from 'moment';
import autobind from 'autobind-decorator';
import * as _ from 'lodash';

import { Button, ButtonSize, ButtonView } from '../../../node_modules/arui-nyc/src/button/button';

import { Transaction } from '../transaction/transaction';
import { Transaction as TransactionModel } from '../../models/models';
import { TransactionsFilter } from '../../state';

import { connect, DispatchProps } from '../../redux';
import * as actions from '../../actions';

import * as theme from 'arui-nyc/src/theme';

const cn = theme.className('transactions');

require('./transactions.styl');

export interface TransactionsProps extends React.DOMAttributes, DispatchProps  {
    lastTransactionMode?: boolean,
    filter?: TransactionsFilter,
    transactions?: TransactionModel[];
}

interface TransactionsState {}

//TODO: move fetching props from state to parent
@connect(appState => {
    return {
        lastTransactionMode: appState.transfersHistory.lastTransactionMode,
        filter: appState.transfersHistory.filter,
        transactions: appState.transfersHistory.transactions
    }
})
export class Transactions extends React.Component<TransactionsProps, TransactionsState> {

    constructor(props) {
        super(props);
    }

    render() {
        return <div className={ cn() }>
            {
                !_.isEmpty(this.props.transactions) && this.props.lastTransactionMode ?
                    this.renderLastTransaction() : this.renderTransactions()
            }
        </div>
    }

    renderTransactions = () => {
        //TODO: move filtering to store
        return this.props.transactions
            .filter(transaction => {
                let result = true;
                if (this.props.filter.startDate) {
                    result = transaction.dateExecute >= this.props.filter.startDate;
                }
                if (result && this.props.filter.endDate) {
                    result = transaction.dateExecute <= this.props.filter.endDate;
                }
                return result;
            })
            .map(transaction => {
                return <Transaction
                    key={ transaction.id }
                    onRepeatClick={ this.onRepeatClick }
                    transaction={ transaction }
                    isFirst={ false }
            />
        });
    }

    renderLastTransaction = () => {
        return <div>
            <Transaction
                transaction={ this.props.transactions[0] }
                onRepeatClick={ this.onRepeatClick } />
            { this.props.transactions.length > 1 &&
                <Button
                    className={ cn('more-btn') }
                    size={ ButtonSize.Large }
                    view={ ButtonView.Pseudo }
                    onClick={ this.onMoreBtnClick } >
                        Показать предыдущие переводы
                </Button>
            }
        </div>
    }

    componentDidMount() {
        this.props.dispatch(actions.loadTransferHistory());
    }

    onMoreBtnClick = () => {
        this.props.dispatch(actions.showMoreTransactions());
    }

    onRepeatClick = (transaction: TransactionModel) => {
        this.props.dispatch(actions.repeatTransaction(transaction));
    }

}
