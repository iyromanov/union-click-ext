import React from 'react';
import { FormField, FormFieldSize, FormFieldView } from 'arui-nyc/src/form-field/form-field';
import { Label, LabelSize } from 'arui-nyc/src/label/label';
import { InputWidth } from 'arui-nyc/src/input/input';
import { AmountInput } from 'arui-nyc/src/amount-input/amount-input';
import * as theme from 'arui-nyc/src/theme';

import { mocks } from '../../mocks/mocks';
import { formatters } from '../../lib/formatters'
import { Account, Amount } from '../../models/models';

require('./credit-amount-field.styl');

const cn = theme.className('credit-amount-field');
let { amountToString } = formatters;

export interface CreditAmountFieldProps {
    account?: Account,
    amount?: Amount,
    error?: string,
    onValueChange: (value: number, formatted: string) => void,
    disabled?: boolean
}

//FIXME: dublicate of DebitAmountField!
export class CreditAmountField extends React.Component<CreditAmountFieldProps, any> {

    render() {
        return (
            <FormField
                size={ FormFieldSize.Large }
                view={ FormFieldView.Compact }
                label={ <Label size={ LabelSize.Large }>Будет зачислено</Label> }
            >
                <AmountInput
                    className={ cn('amount-input') }
                    width={ InputWidth.Available }
                    rightAddons={ [
                        <div className={ cn('currency') } >{ this.props.account && this.props.account.currency.symbol }</div>
                    ] }
                    value={ amountToString(this.props.amount) }
                    onValueChange={ this.handleAmountChange }
                    error={ this.props.error }
                    disabled={ this.props.disabled }
                    />
            </FormField>
        );
    }

    handleAmountChange = (value: number, formatted: string) => {
        this.props.onValueChange(value, formatted);
    }

}
