import React from 'react';
import { Heading, HeadingLevel } from 'arui-nyc/src/heading/heading'
import * as theme from 'arui-nyc/src/theme';

import { Section } from '../section/section';

require('./app-heading.styl');

const cn = theme.className('app-heading');

export class AppHeading extends React.Component<any, any> {

    render() {
        return <div className={ cn() }>
            <Section content={
                <Heading lvl={ HeadingLevel.H1 }>Перевод в Альфа-Банк Беларусь</Heading>
            }/>
        </div>
    }

}
