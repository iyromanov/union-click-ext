import React from 'react';
import { FormField, FormFieldSize, FormFieldView } from 'arui-nyc/src/form-field/form-field'
import { Label, LabelSize } from 'arui-nyc/src/label/label'
import { Input, InputWidth, InputRuntime } from 'arui-nyc/src/input/input'
import { Radio, RadioType, RadioSize } from 'arui-nyc/src/radio/radio';
import { RadioGroup, RadioGroupType, RadioGroupSize } from 'arui-nyc/src/radio-group/radio-group';
import * as theme from 'arui-nyc/src/theme';

import * as actions from '../../actions';
import { config, RecipientSwitcherButtonConfig } from '../../config/config';
import { Account, Recipient, RecipientType } from '../../models/models';

require('./recipient-field.styl');

const cn = theme.className('recipient-field');

export interface RecipientFieldProps {
    recipient?: Recipient,
    error?: string,
    nameError?: string,
    disabled?: boolean,
    focused?: boolean,
    nameFocused?: boolean,
    onTypeChange?: (value: RecipientType) => void,
    onValueChange?: (value: string) => void
    onNameChange?: (value: string) => void
}

export interface RecipientFieldState {
    value?: string,
    name?: string,
    error?: string
}

export class RecipientField extends React.Component<RecipientFieldProps, RecipientFieldState> {

    refs: {
       [key: string]: React.Component<any, any>;
       recipientInput: React.Component<any, any> & InputRuntime;
       recipientNameInput: React.Component<any, any> & InputRuntime;
    }

    constructor(props) {
        super(props);
        this.state = this.getInitState(props);
    }

    componentWillReceiveProps(props) {
        this.setState(this.getInitState(props));
    }

    componentDidUpdate() {
        this.props.focused && this.refs.recipientInput.focus();
        this.props.nameFocused && this.refs.recipientNameInput.focus();
    }

    render() {
        return (
            <div className={ cn() }>
                <FormField
                    className={ cn('switcher-field') }
                    size={ FormFieldSize.Large }
                    view={ FormFieldView.Compact }
                    label={
                        <Label size={ LabelSize.Large }>Выберите тип реквизита для перевода</Label>
                    }>
                        { this.renderSwitcher() }
                </FormField>
                <FormField
                    className={ cn('common-field') }
                    size={ FormFieldSize.Large }
                    view={ FormFieldView.Compact }
                    label={
                        <Label size={ LabelSize.Large }>{ this.getHint() }</Label>
                    }
                >
                    { this.renderCommonInput() }
                    <Label size={ LabelSize.Medium }>
                        { this.props.recipient.account ? 'Счет в белорусских рублях,  Ш....ер Д.А.' : '' }
                    </Label>
                </FormField>
                { this.props.recipient.type == RecipientType.PhoneNumber &&
                    <FormField
                        className={ cn('name-field') }
                        size={ FormFieldSize.Large }
                        view={ FormFieldView.Compact }
                        label={
                            <Label size={ LabelSize.Large }>Фамилия получателя</Label>
                        }
                    >
                        <Input
                            width={ InputWidth.Available }
                            onChange={ this.onNameChange }
                            onBlur={ this.handleNameChange }
                            value={ this.state.name }
                            placeholder='Иванов'
                            pattern='ccccccccccccccccccccccccccccccccc'
                            error={ this.props.nameError }
                            disabled={ this.props.disabled }
                            ref={ 'recipientNameInput' }
                        />
                    </FormField>
                }
            </div>
        );
    }

    renderSwitcher(): React.ReactElement<any> {
        return (
            <RadioGroup
                size={ RadioGroupSize.Medium }
                groupType={ RadioGroupType.Button }
            >
                { this.getRadioButtons() }
            </RadioGroup>
        )
    }

    getRadioButtons(): React.ReactElement<any>[] {
        return config.recipientSwitcherButtons.map(button => {
            return (
                <Radio
                    key={ 'radios' + button.type }
                    size={ RadioSize.Large }
                    radioType={ RadioType.Button }
                    checked={ this.props.recipient.type === button.type }
                    onClick={ () => {
                        if (!this.props.disabled) {
                            this.onRecipientTypeChange(button.type);
                        }
                    } }
                >
                   { button.name }
               </Radio>
           )
        });
    }

    //TODO: workaround because of ARUI-87. refact this!
    renderCommonInput = (): React.ReactElement<any> => {
        return (
            <div>
                { this.props.recipient.type == RecipientType.Account &&
                    <Input
                        width={ InputWidth.Available }
                        onChange={ this.onValueChange }
                        onBlur={ this.handleRecipientValueChange }
                        value={ this.state.value }
                        error={ this.state.error }
                        pattern={ this.getPattern() }
                        disabled={ this.props.disabled }
                        ref={ 'recipientInput' }
                    />
                }
                { this.props.recipient.type == RecipientType.PhoneNumber &&
                    <Input className={ cn('phone-input') }
                        width={ InputWidth.Available }
                        onChange={ this.onValueChange }
                        onBlur={ this.handleRecipientValueChange }
                        value={ this.state.value }
                        leftAddons={[
                            <span>+375</span>
                        ]}
                        error={ this.state.error }
                        pattern={ this.getPattern() }
                        disabled={ this.props.disabled }
                        ref={ 'recipientInput' }
                    />
                }
                { this.props.recipient.type == RecipientType.Card &&
                    <Input
                        width={ InputWidth.Available }
                        onChange={ this.onValueChange }
                        onBlur={ this.handleRecipientValueChange }
                        value={ this.state.value }
                        error={ this.state.error }
                        pattern={ this.getPattern() }
                        disabled={ this.props.disabled }
                        ref={ 'recipientInput' }
                    />
                }
            </div>
        );
    }

    onValueChange = (val: string): void => {
        this.setState({
            value: val
         });
    }

    onNameChange = (val: string): void => {
        this.setState({
            name: val
        });
    }

    onRecipientTypeChange = (val: RecipientType): void => {
        this.props.onTypeChange(val);
    }

    handleRecipientValueChange = () => {
        this.props.onValueChange(this.state.value.replace(/ /g,''));
    }

    handleNameChange = () => {
        this.props.onNameChange(this.state.name);
    }

    getPattern = (): string => {
        return config.recipientSwitcherButtons.filter(b => {
            return b.type == this.props.recipient.type;
        })[0].pattern;
    }

    getHint = (): string => {
        return config.recipientSwitcherButtons.filter(b => {
            return b.type == this.props.recipient.type;
        })[0].hint;
    }

    getInitState(props: RecipientFieldProps): RecipientFieldState {
        let userValue = props.recipient.userValues[props.recipient.type];
        return {
            value: userValue ? userValue.value : '',
            error: userValue ? userValue.error : '',
            name: props.recipient.name || ''
        }
    }
}
