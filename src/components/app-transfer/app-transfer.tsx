import React from 'react';
import { Label, LabelSize } from 'arui-nyc/src/label/label';
import { Input, InputWidth } from 'arui-nyc/src/input/input';
import { AmountInput } from 'arui-nyc/src/amount-input/amount-input';
import { Button, ButtonSize, ButtonView } from 'arui-nyc/src/button/button';
import { StringSelect, SingleOption, SelectWidth } from 'arui-nyc/src/select/select';
import { FormField, FormFieldView, FormFieldSize } from 'arui-nyc/src/form-field/form-field';
import * as theme from 'arui-nyc/src/theme';
import { spread } from 'arui/src/lib/assign';
import { formatters as formattersExt } from 'alfa-components/lib/formatters';

import { Section } from '../section/section';
import { Row } from '../row/row';
import { RowColumn, RowColumnSize } from '../row/__column/row__column';
import { RecipientField } from '../recipient-field/recipient-field'
import { DebitAccountField } from '../debit-account-field/debit-account-field'
import { DebitAmountField } from '../debit-amount-field/debit-amount-field'
import { CreditAmountField } from '../credit-amount-field/credit-amount-field'
import { TargetField } from '../target-field/target-field'
import { SmsField } from '../sms-field/sms-field'
import { ExchangeRateHint } from '../exchange-rate-hint/exchange-rate-hint';
import { mocks } from '../../mocks/mocks';
import { connect, DispatchProps } from '../../redux';
import * as actions from '../../actions';
import { Account, Amount, Recipient, RecipientType } from '../../models/models';

require('./app-transfer.styl');

const cn = theme.className('app-transfer');
let { deformatAmount } = formattersExt;

export interface AppTransferProps extends DispatchProps {
    debitAccount?: Account,
    debitAmount?: Amount,
    feeAmount?: Amount,
    debitAmountFieldError?: string,
    recipient?: Recipient,
    recipientFieldError?: string,
    recipientNameFieldError?: string,
    creditAmount?: Amount,
    creditAmountFieldError?: string,
    isValid?: boolean,
    isReadOnly?: boolean,
    isSmsCodeSending?: boolean
}

interface AppTransferState {
    reason?: string
}

@connect(appState => {
    return {
        debitAccount: appState.transfer.debitAccount,
        debitAmount: appState.transfer.debitAmount,
        debitAmountFieldError: appState.transfer.debitAmountFieldError,
        recipient: appState.transfer.recipient,
        recipientFieldError: appState.transfer.recipientFieldError,
        recipientNameFieldError: appState.transfer.recipientNameFieldError,
        creditAmount: appState.transfer.creditAmount,
        creditAmountFieldError: appState.transfer.creditAmountFieldError,
        feeAmount: appState.transfer.feeAmount,
        isValid: appState.transfer.isValid,
        isReadOnly: appState.transfer.isReadOnly,
        isSmsCodeSending: appState.transfer.isSmsCodeSending
    }
})
export class AppTransfer extends React.Component<AppTransferProps, AppTransferState> {

    constructor(props) {
        super(props);
        this.state = {
            reason: mocks.reasons[0]
        }
    }

    render() {
        return <div className={ cn() }>
            <Section title='Кому' content={
                <Row>
                    <RowColumn>
                        <RecipientField
                            recipient={ this.props.recipient }
                            error={ this.props.recipientFieldError }
                            nameError={ this.props.recipientNameFieldError }
                            disabled={ this.props.isReadOnly }
                            focused={ this.isRecipientFocused() }
                            nameFocused={ this.isRecipientNameFocused() }
                            onTypeChange={ this.onRecipientTypeChange }
                            onValueChange={ this.onRecipientAccountChange }
                            onNameChange={ this.onRecipientNameChange }
                        />
                    </RowColumn>
                    <RowColumn>
                        {
                            this.showExchangeRate() &&
                                <ExchangeRateHint
                                    firstAmount={ {
                                        value: this.props.debitAccount.balance,
                                        currency: this.props.debitAccount.currency
                                    } }
                                    secondAmount={ mocks.secondAmount }
                                    feeAmount={ this.props.feeAmount }
                                />
                        }
                    </RowColumn>
                </Row>
            } />
            <Section className={ cn('debit') }
                    title='Откуда' content={
                <Row>
                    <RowColumn>
                        <DebitAccountField disabled={ this.props.isReadOnly }/>
                    </RowColumn>
                </Row>
            } />
            <Section title='Сколько'
                    content={
                <Row>
                    <RowColumn size={ RowColumnSize.Small }>
                        <DebitAmountField
                            account={ this.props.debitAccount }
                            amount={ this.props.debitAmount }
                            error={ this.props.debitAmountFieldError }
                            onValueChange={ this.onDebitAmountChange }
                            focused={ this.isDebitAmountFocused() }
                            disabled={ !this.isValidTransfer() || this.props.isReadOnly }
                        />
                    </RowColumn>
                    <RowColumn size={ RowColumnSize.Small }>
                        <CreditAmountField
                            account={ this.props.recipient && this.props.recipient.account }
                            amount={ this.props.creditAmount }
                            error={ this.props.creditAmountFieldError }
                            onValueChange={ this.onCreditAmountChange }
                            disabled={ !this.isValidTransfer() || this.props.isReadOnly }
                        />
                    </RowColumn>
                </Row>
            } />
            <Section className={ cn('target') }
                title='Назначение' content={
                    <Row>
                        <RowColumn>
                            <TargetField
                                disabled={ this.props.isReadOnly }
                            />
                        </RowColumn>
                    </Row>
            } />
            { !this.props.isReadOnly &&
                <Section className={ cn('transfer-btn') }
                        content={
                    <Row>
                        <RowColumn size={ RowColumnSize.Small }>
                            <Button
                                size={ ButtonSize.Large }
                                onClick={ this.onTransferClick }
                                view={ ButtonView.Action } >
                                    Перевести
                            </Button>
                        </RowColumn>
                    </Row>
                } />
            }
            { this.props.isReadOnly &&
                <Section content={
                    <SmsField
                        onCodeEntered={ this.onSmsCodeEntered }
                        isSending={ this.props.isSmsCodeSending }
                        onBackButtonClick={ this.onBackButtonClick }
                    />
                } />
            }
        </div>
    }

    onTransferClick = (): void => {
        this.props.dispatch(actions.validateTransfer());
    }

    onDebitAmountChange = (value: number, formatted: string): void => {
        let valueWithMinority = value * this.props.debitAccount.currency.minorNumber;
        this.props.dispatch(actions.changeDebitAmount(valueWithMinority));
    }

    onCreditAmountChange = (value: number, formatted: string): void => {
        let valueWithMinority = value * this.props.recipient.account.currency.minorNumber;
        this.props.dispatch(actions.changeCreditAmount(valueWithMinority));
    }

    isValidTransfer = (): boolean => {
        //TODO: move to app state
        return (!!this.props.debitAccount && !!this.props.recipient.account);
    }

    isRecipientFocused = (): boolean => {
        return !!this.props.recipient.userValues[this.props.recipient.type].error;
    }

    isRecipientNameFocused = (): boolean => {
        return !this.isRecipientFocused() && !!this.props.recipientNameFieldError;
    }

    isDebitAmountFocused = (): boolean => {
        return !this.isRecipientNameFocused() && !!this.props.debitAmountFieldError;
    }

    showExchangeRate = (): boolean => {
        return !!this.props.recipient.account;
    }

    onSmsCodeEntered = (code: string): void => {
        this.props.dispatch(actions.enterSmsCode(code));
    }

    onRecipientTypeChange = (type: RecipientType): void => {
        this.props.dispatch(actions.changeRecipientType(type));
    }

    onRecipientAccountChange = (value: string): void => {
        this.props.dispatch(actions.changeRecipientValue(value));
    }

    onRecipientNameChange = (value: string): void => {
        this.props.dispatch(actions.changeRecipientName(value));
    }

    onBackButtonClick = (): void => {
        this.props.dispatch(actions.unlockForm());
    }

}
