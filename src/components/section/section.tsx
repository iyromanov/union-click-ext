import React from 'react';
import { Label, LabelSize } from 'arui-nyc/src/label/label'
import * as theme from 'arui-nyc/src/theme';

const cn = theme.className('section');

require('./section.styl');

export interface SectionProps extends React.CommonAttributes {
    title?: string
    content: React.ReactElement<any>,
    hidden?: boolean
}

export class Section extends React.Component<SectionProps, any> {

    render() {
        return <div className={ cn({ hidden: this.props.hidden }).mix(this.props.className) }>
            <div className={ cn('title') }>
                <Label size={ LabelSize.Large }>{ this.props.title }</Label>
            </div>
            <div className={ cn('content') }>{ this.props.content }</div>
        </div>
    }

}
