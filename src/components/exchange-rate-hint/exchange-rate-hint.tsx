import React from 'react';
import { Label, LabelSize } from 'arui-nyc/src/label/label'
import { Heading, HeadingLevel } from 'arui-nyc/src/heading/heading'
import * as theme from 'arui-nyc/src/theme';

import { Amount } from '../../models/models';
import { formatters } from '../../lib/formatters';
import * as actions from '../../actions';
import { ExchangeService } from '../../services/ExchangeService';

const cn = theme.className('exchange-rate-hint');
let { amountWithCurrencyToString } = formatters;

require('./exchange-rate-hint.styl');

export interface ExchangeRateHintProps {
    firstAmount: Amount,
    secondAmount: Amount,
    feeAmount?: Amount
}

export class ExchangeRateHint extends React.Component<ExchangeRateHintProps, any> {

    render() {
        let firstAmount = amountWithCurrencyToString(this.props.firstAmount),
            secondAmount = amountWithCurrencyToString(this.props.secondAmount),
            feeAmount = amountWithCurrencyToString(this.props.feeAmount);
        return <div className={ cn() }>
            { !this.isEqualCurrencies() &&
                <div className={ cn('exchange') }>
                    <Heading lvl={ HeadingLevel.H4 }>Курс обмена</Heading>
                    <Label size={ LabelSize.Large }>
                        { ExchangeService.getFormattedExchangeRate(this.props.firstAmount.currency, this.props.secondAmount.currency) }
                    </Label>
                </div>
            }
            { this.props.feeAmount && this.props.feeAmount.value != 0 &&
                <div className={ cn('fee') }>
                    <Heading lvl={ HeadingLevel.H4 }>Комиссия</Heading>
                    <Label size={ LabelSize.Large }>{ `${feeAmount}` }</Label>
                </div>
            }
        </div>
    }

    isEqualCurrencies = () => {
        //TODO: remove hack
        return this.props.firstAmount.currency.symbol === this.props.secondAmount.currency.symbol;
    }

}
