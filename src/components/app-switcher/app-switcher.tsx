import React from 'react';
import { Heading, HeadingLevel } from 'arui-nyc/src/heading/heading';
import { Link, LinkSize } from 'arui-nyc/src/link/link';
import * as theme from 'arui-nyc/src/theme';

import { Section } from '../section/section';
import { config, MenuItemConfig } from '../../config/config';

require('./app-switcher.styl');

const cn = theme.className('app-switcher');

export interface AppSwitcherProps extends React.DOMAttributes {
    location?: string,
    onSwitcherClick?: (location: string) => void
}

export class AppSwitcher extends React.Component<AppSwitcherProps, any> {

    static defaultProps = {
        location: '/'
    }

    render() {
        return <div className={ cn() }>
            <Section content={
                <div>{ this.renderLinks(config.switcherLinks) }</div>
            } />
        </div>
    }

    renderLinks = (items: MenuItemConfig[]): React.ReactElement<any>[] => {
        //TODO: render as menu
        return items.map((item: MenuItemConfig, index: number) => {
            if (item.link === this.props.location) {
                return <Link size={ LinkSize.Large } key={ `${index}` }>{ item.title }</Link>
            } else {
                return <Link size={ LinkSize.Large }
                    text={ item.title }
                    pseudo={ true }
                    key={ `${index}` }
                    onClick={ () => { this.props.onSwitcherClick(item.link) } } />
            }
        });
    }

}
