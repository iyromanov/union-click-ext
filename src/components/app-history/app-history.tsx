import React from 'react';;
import * as theme from 'arui-nyc/src/theme';

import { Section } from '../section/section';
import { TransactionsFilter } from '../transactions-filter/transactions-filter';
import { Transactions } from '../transactions/transactions';
import { Transaction } from '../../models/models';
import { TransactionsFilter as TransactionsFilterModel } from '../../state';
import * as actions from '../../actions';
import { connect, DispatchProps } from '../../redux';

require('./app-history.styl');

const cn = theme.className('app-history');

export interface AppHistoryProps extends DispatchProps {
    filter?: TransactionsFilterModel,
    transactions?: Transaction[],
    lastTransactionMode?: boolean
}

@connect(appState => {
    return {
        filter: appState.transfersHistory.filter,
        transactions: appState.transfersHistory.transactions,
        lastTransactionMode: appState.transfersHistory.lastTransactionMode
    }
})
export class AppHistory extends React.Component<AppHistoryProps, any> {

    render() {
        return <div className={ cn() }>
            <Section content={
                <TransactionsFilter
                    filter={ this.props.filter }
                    transactions={ this.props.transactions }
                    lastTransactionMode={ this.props.lastTransactionMode }
                    onUpdateFilter={ this.onUpdateFilter }
                />
            } />
            <Section content={
                <Transactions />
            } />
        </div>
    }

    onUpdateFilter = (filter: TransactionsFilterModel) => {
        this.props.dispatch(actions.changeTransactionsFilter(filter));
    }

}
