import React from 'react';
import { Label, LabelSize } from 'arui-nyc/src/label/label'
import * as theme from 'arui-nyc/src/theme';

const cn = theme.className('section');

require('./section.styl');

export interface SectionProps {
    title?: string,
    content: React.ReactElement<any>
}

export class Section extends React.Component<SectionProps, any> {

    render() {
        return <div className={ cn() }>
            <div className={ cn('title') }>
                <Label size={ LabelSize.ExtraLarge }>{ this.props.title }</Label>
            </div>
            <div className={ cn('content') }>{ this.props.content }</div>
        </div>
    }

}
