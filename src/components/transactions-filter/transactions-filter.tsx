import React from 'react';
import moment from 'moment';
import { spread } from 'arui/src/lib/assign';
import * as _ from 'lodash';

import { CalendarInput, InputSize } from 'arui-nyc/src/calendar-input/calendar-input';
import { PeriodPicker } from 'arui-nyc/src/period-picker/period-picker';
import { ButtonSize } from 'alfa-components/button/button'

import { SearchBar } from '../ui/search-bar/search-bar';
import { TransactionsFilter as TransactionsFilterModel } from '../../state';
import { Transaction } from '../../models/models';

import * as theme from 'arui-nyc/src/theme';

const cn = theme.className('transactions-filter');
type Moment = moment.Moment;

export interface TransactionsFilterProps extends React.DOMAttributes {
    filter?: TransactionsFilterModel,
    onUpdateFilter: (filter: TransactionsFilterModel) => void,
    transactions?: Transaction[],
    lastTransactionMode?: boolean
}

interface TransactionsFilterState {
    startDate?: Moment;
    endDate?: Moment;
    minDate?: Moment;
    maxDate?: Moment;
}

export class TransactionsFilter extends React.Component<TransactionsFilterProps, TransactionsFilterState> {

    constructor(props) {
        super(props);
        this.state = this.calculateState(props);
    }

    componentWillReceiveProps(props: TransactionsFilterProps) {
        this.setState(this.calculateState(props));
    }

    render() {
        return <div className={ cn() }>
            <SearchBar
                className={ cn('search') }
                onSearch={ this.handleSearch }
                placeholder={ 'Поиск по номеру телефона или счёта' }
            />
            <PeriodPicker
                className={ cn('period') }
                size={ ButtonSize.Large }
                leftPlaceholder="Не выбрано"
                rightPlaceholder="Не выбрано"
                showButtonText="Показать"
                valueFrom={ this.state.startDate }
                valueTo={ this.state.endDate }
                period={{
                    showTimeline: false,
                    inputFromPlaceholder: 'Дата начала',
                    inputToPlaceholder: 'Дата окончания',
                    minDate: this.state.minDate,
                    maxDate: this.state.maxDate
                }}
                onPeriodChange={ this.handlePeriodChange }
                onShow={ this.handlePeriodShow }
            />
        </div>

    }

    handleSearch = () => {
        console.log('searching');
    }

    handlePeriodChange = (startDate: Moment, endDate: Moment) => {
        this.setState({
            startDate: startDate,
            endDate: endDate && endDate.endOf('day')
        });
    }

    handlePeriodShow = () => {
        let { startDate, endDate } = this.state;
        //TODO: replace selected value on null. is possible?
        this.props.onUpdateFilter({
            startDate,
            endDate
        });
    }

    calculateState = (props: TransactionsFilterProps): any => {
        if (_.isEmpty(props.transactions)) {
            return {}
        }
        return {
            startDate: props.filter.startDate,
            endDate: props.filter.endDate,
            minDate: _.min(props.transactions, 'dateExecute').dateExecute,
            maxDate: _.max(props.transactions, 'dateExecute').dateExecute
        }
    }

}

require('./transactions-filter.styl');
