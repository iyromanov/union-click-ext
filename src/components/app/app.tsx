import React from 'react';

import { Page } from 'arui-nyc/src/page/page'
import { Header, HeaderInner } from 'arui-nyc/src/header/header';
import { Footer, FooterInner } from 'arui-nyc/src/footer/footer';
import { Menu } from 'arui-nyc/src/menu/menu';
import { MenuItem, MenuItemType } from 'arui-nyc/src/menu-item/menu-item';
import { Link, LinkSize } from 'arui-nyc/src/link/link';
import * as theme from 'arui-nyc/src/theme';

import { AppHeading } from '../app-heading/app-heading';
import { AppSwitcher } from '../app-switcher/app-switcher';
import { AppTransfer } from '../app-transfer/app-transfer';
import { AppHistory } from '../app-history/app-history';
import { connect, DispatchProps } from '../../redux';
import * as actions from '../../actions';

import { config, MenuItemConfig } from '../../config/config';

require('./app.styl');

const cn = theme.className('app');

interface AppProps extends React.DOMAttributes, DispatchProps {
    location?: string
}

interface AppState extends React.DOMAttributes {}

@connect(appState => {
    return {
        location: appState.transfer.location
    }
})
export class App extends React.Component<AppProps, AppState> {

    constructor(props) {
        super(props);
    }

    render() {
        return <theme.ThemeProvider theme={ theme.ThemeType.Color }>
            <Page
                header={ this.renderHeader() }
                footer={ this.renderFooter() }>
                { this.renderApp() }
            </Page>
        </theme.ThemeProvider>;
    }

    renderHeader() {
        return (
            <Header
                inner={
                    <HeaderInner
                        href='#'
                        menu={
                            <Menu>{ this.renderMenuItems(config.menuHeader) }</Menu>
                        }
                    />
                }
            />
        );
    }

    renderFooter() {
        return (
            <Footer
                inner={
                    <FooterInner
                        menu={
                            <Menu>{ this.renderMenuItems(config.menuFooter) }</Menu>
                        }
                    />
                }
            />
        );
    }

    renderApp() {
        return <div className={ cn() }>
            <AppHeading />
            <AppSwitcher location={ this.props.location } onSwitcherClick={ this.onSwitcherClick } />
            { this.renderContent() }
        </div>
    }

    renderContent() {
        switch(this.props.location) {
            case '/history':
                return <AppHistory />;
            default:
                return <AppTransfer />;
        }
    }

    renderMenuItems(items: MenuItemConfig[]): React.ReactElement<any>[] {
        return items.map((item, index) => {
            return <MenuItem menuItemType={ MenuItemType.Link }
                key={ 'menu' + index }
                horizontal={ true }>
                    <Link href={ item.link } text={ item.title } />
            </MenuItem>
        });
    }

    onSwitcherClick = (location: string) => {
        this.props.dispatch(actions.changeLocation(location));
    }

}
