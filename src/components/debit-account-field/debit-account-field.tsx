import React from 'react';
import { FormField, FormFieldSize, FormFieldView } from 'arui-nyc/src/form-field/form-field';
import { Label, LabelSize } from 'arui-nyc/src/label/label';
import { Input, InputWidth } from 'arui-nyc/src/input/input';
import { StringSelect, SingleOption, SelectWidth } from 'arui-nyc/src/select/select';
import * as theme from 'arui-nyc/src/theme';

import { Account } from '../../models/models';
import { AccountSelect } from '../ui/account-select/account-select'
import { connect, DispatchProps } from '../../redux';
import * as actions from '../../actions';

const cn = theme.className('debit-account-field');

export interface DebitAccountFieldProps extends DispatchProps {
    account?: Account,
    accounts?: Account[],
    disabled?: boolean
}

@connect(appState => {
    return {
        account: appState.transfer.debitAccount,
        accounts: appState.transfer.debitAccounts
    }
})
export class DebitAccountField extends React.Component<DebitAccountFieldProps, any> {

    render() {
        return (
            <FormField
                className={ cn() }
                size={ FormFieldSize.Large }
                view={ FormFieldView.Compact }
            >
                <AccountSelect
                    accounts={ this.props.accounts }
                    selectedAccount={ this.props.account }
                    handleAccountSelect={ this.handleDebitAccountChange }
                    disabled={ this.props.disabled }
                />
            </FormField>
        );
    }

    handleDebitAccountChange = (option: SingleOption<string>) => {
        this.props.dispatch(actions.changeDebitAccount(option.value));
    }

}
