import React from 'react';
import autobind from 'autobind-decorator';
import moment from 'moment';

import { formatters as baseFormatters } from 'alfa-components/lib/formatters';
import { StringSelect, SingleOption, SelectWidth } from 'arui-nyc/src/select/select';
import { Label, LabelSize } from 'arui-nyc/src/label/label'
import { Icon, IconSize } from 'arui-nyc/src/icon/icon';
import { Button, ButtonSize, ButtonView } from 'arui-nyc/src/button/button';
import { Link, LinkSize } from 'arui-nyc/src/link/link';
import * as theme from 'arui-nyc/src/theme';

import { Transaction as TransactionModel, Amount, RecipientType } from '../../models/models';
import { formatters } from '../../lib/formatters';
import { TargetField } from '../target-field/target-field';
import { mocks } from '../../mocks/mocks';


const cn = theme.className('transaction');

export interface TransactionProps extends React.DOMAttributes, theme.ThemeProps {
    transaction: TransactionModel;
    onRepeatClick: (transaction: TransactionModel) => void;
    isFirst?: boolean;
}

interface TransactionState {
    hovered?: boolean;
    expanded?: boolean;
    costItem?: string
}

export class Transaction extends React.Component<TransactionProps, TransactionState> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            hovered: false,
            expanded: false,
            costItem: mocks.costItems[0]
        };
    }

    shouldComponentUpdate(nextProps: TransactionProps, nextState: TransactionState): boolean {
        return (
            this.props.isFirst !== nextProps.isFirst
            || this.state.hovered !== nextState.hovered
            || this.state.expanded !== nextState.expanded
        );
    }

    render() {
        let { transaction, isFirst } = this.props;
        return (
            <li className={ cn({ expanded: this.state.expanded }) }
                role='transaction' key={ `transaction-${transaction.id}`}
                onMouseEnter={ this.handleOnHover }
                onMouseLeave={ this.handleOnHover }
                >
                { this.renderMainInfoBlock() }
                { this.renderAdditionalInfoBlock() }
            </li>
        );
    }

    renderMainInfoBlock() {
    return <div className={ cn('main-info') }>
        { this.renderTransferStatusInfo() }
        { this.renderTransferFieldsName() }
        { this.renderTransferFieldsValue() }
        { this.renderRepeatTransferButton() }
    </div>
    }

    renderTransferStatusInfo() {
        let { transaction } = this.props;
        return <div className={ cn('transaction-col') } style={ { width: '30%' } }>
            <div className={ cn('time') }>
                <Label size={ LabelSize.Large }>{ formatters.formatDate(transaction.dateExecute) }</Label>
                <Label size={ LabelSize.Large }>{ formatters.formatTime(transaction.dateExecute) }</Label>
            </div>
                <div className={ cn('status') }>
                    <Icon size={ IconSize.Small } action={ transaction.status.toString() }/>
                    <Label size={ LabelSize.Large }>{ formatters.formatStatus(transaction.status) }</Label>
                </div>
            </div>
    }

    renderTransferFieldsName() {
        return <div className={ cn('transaction-col') } style={ { width: '20%' } }>
            <div className={ cn('field-label') }>
                <Label size ={ LabelSize.Large } >Счёт отправителя</Label>
                <Label size ={ LabelSize.Large } >Получатель</Label>
                <Label size ={ LabelSize.Large } >Сумма перевода</Label>
            </div>
        </div>
    }

    renderTransferFieldsValue() {
        let { transaction } = this.props;
        let receiver = transaction.recipient.userValues[transaction.recipient.type].value;
        receiver = transaction.recipient.type == RecipientType.PhoneNumber ? '+375' + receiver : receiver;
        return <div className={ cn('transaction-col') } style={ { width: '20%' } }>
            <div className={ cn('field-value') }>
                <Label size ={ LabelSize.Large } >{ transaction.debitAccount.number }</Label>
                <Label size ={ LabelSize.Large } >{ receiver }</Label>
                <Label size ={ LabelSize.Large } >{ formatters.amountWithCurrencyToString(transaction.sum) }</Label>
            </div>
        </div>
    }

    renderRepeatTransferButton() {
    return <div className={ cn('transaction-col') } style={ { width: '20%' } }>
        <div className={ cn('repeat-payment') }>
            <Button size={ ButtonSize.Large }
                view={ ButtonView.Action }
                onClick={ this.onRepeatClick }>
            {
                [
                    'Повторить перевод',
                    <Icon size={ IconSize.Small } action={ 'repeat' }/>
                ]
            }
            </Button>
        </div>
    </div>
    }

    renderShowDetailsLink() {
        return <Link
            className={ cn('more') }
            size={ LinkSize.Medium }
            text={ !this.state.expanded ? 'Подробнее' : 'Скрыть подробности' }
            pseudo={ true }
            onClick={ this.handleShowAdditionalInfo }>
        </Link>
    }

    renderAdditionalInfoBlock() {
        let { transaction } = this.props;
        return <div className={ cn('additional-info') }>
            <div className={ cn('transaction-col') } style={ { width: '30%' } }/>
            <div className={ cn('transaction-col') } style={ { width: '40%' } }>
                { !this.state.expanded && this.renderShowDetailsLink() }
                { this.state.expanded &&
                <div className={ cn('additional-fields') }>
                    <div className={ cn('transaction-col') } style={ { width: '50%' } }>
                        <div className={ cn('field-label') }>
                                <Label size ={ LabelSize.Large } >Комиссия</Label>
                                <Label size ={ LabelSize.Large } >Сумма с комиссией</Label>
                                <Label size ={ LabelSize.Large } >Код авторизации</Label>
                                <div style={ { marginTop: '30px' } }>
                                    <Label size ={ LabelSize.Large } >Статья расхода</Label>
                                </div>
                        </div>
                    </div>
                    <div className={ cn('transaction-col') } style={ { width: '50%' } }>
                        <div className={ cn('field-value') }>
                            <Label size ={ LabelSize.Large } >{ formatters.amountWithCurrencyToString(transaction.fee) }</Label>
                            <Label size ={ LabelSize.Large } >
                            { formatters.amountWithCurrencyToString(this.calculateAmountWithCommission(transaction.sum, transaction.fee)) }
                            </Label>
                            <Label size ={ LabelSize.Large } >{ transaction.authorizationCode }</Label>
                            <StringSelect
                                options={ this.formatOptions(mocks.costItems, this.state.costItem) }
                                width={ SelectWidth.Available }
                                onOptionClick={ (option: SingleOption<string>): void => {
                                    this.setState({ costItem: option.value });
                                } }
                            />
                        </div>
                    </div>
                    <TargetField disabled={ true } />
                    { this.renderToolButtons() }
                    <div className={ cn('hide')}>
                        { this.renderShowDetailsLink() }
                    </div>
                </div>
                }
            </div>
        </div>
    }

    renderToolButtons() {
        return <div className={ cn('tool-buttons') }>
            <Button view={ ButtonView.Pseudo } size={ ButtonSize.Large }>
            {
                [
                    'Печать',
                    <Icon size={ IconSize.Small } tool={ 'pr' }/>
                ]
            }
            </Button>
            <Button view={ ButtonView.Pseudo } size={ ButtonSize.Large }>
            {
                [
                    'Отправить',
                    <Icon size={ IconSize.Small } tool={ 'email' }/>
                ]
            }
            </Button>
        </div>
    }

    onRepeatClick = () => {
        this.props.onRepeatClick(this.props.transaction);
    }

    private handleShowAdditionalInfo = (e: React.MouseEvent) => {
        this.setState({ expanded: !this.state.expanded });
    }

    handleOnHover = (e: React.MouseEvent) => {
        this.setState({ hovered: !this.state.hovered });
    }

    private calculateAmountWithCommission(sum: Amount, commission: Amount): Amount {
        if (!sum) {
            return null;
        }
        //TODO: what to do if currencies are different??
        let totalAmount: Amount = {
            value: sum.value + (commission ? commission.value : 0),
            currency: sum.currency
        };

        return totalAmount;
    }

    formatOptions(options: string[], value: string): SingleOption<string>[] {
        return options.map((option: string) => {
            return {
                value: option,
                checked: option == value
            }
        });
    }


}

require('./transaction.styl');
