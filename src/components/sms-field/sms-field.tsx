import React from 'react';
import { FormField, FormFieldSize, FormFieldView } from 'arui-nyc/src/form-field/form-field';
import { Label, LabelSize } from 'arui-nyc/src/label/label';
import { Input, InputWidth, InputRuntime } from 'arui-nyc/src/input/input';
import { StringSelect, SingleOption, SelectWidth } from 'arui-nyc/src/select/select';
import { Button, ButtonSize, ButtonView } from 'arui-nyc/src/button/button';
import { Spin, SpinSize } from 'arui-nyc/src/spin/spin';
import { Link, LinkSize } from 'arui-nyc/src/link/link';
import * as theme from 'arui-nyc/src/theme';

import { Row } from '../row/row';
import { RowColumn, RowColumnSize } from '../row/__column/row__column';
import { config } from '../../config/config';

const cn = theme.className('sms-field');

require('./sms-field.styl');

export interface SmsFieldProps {
    onCodeEntered: (code: string) => void,
    isSending?: boolean,
    onBackButtonClick?: () => void
}

export interface SmsFieldState {
    smsCode: string
}

export class SmsField extends React.Component<SmsFieldProps, SmsFieldState> {

    constructor(props) {
        super(props);
        this.state = {
            smsCode: ''
        }
    }

    refs: {
       [key: string]: React.Component<any, any>;
       smsInput: React.Component<any, any> & InputRuntime;
   }

   componentDidMount() {
       this.refs.smsInput.focus();
   }

    render() {
        return (
            <div className={ cn() }>
                <Row>
                    <RowColumn>
                        <Label size={ LabelSize.Large }>Одноразовый пароль выслан вам на телефон</Label>
                    </RowColumn>
                </Row>
                <Row>
                    <RowColumn size={ RowColumnSize.Small }>
                        <FormField
                            size={ FormFieldSize.Large }
                            view={ FormFieldView.Compact }
                        >
                            <Input
                                width={ InputWidth.Available }
                                onChange={ (val) => { this.setState({ smsCode: val }) } }
                                onKeyDown={ this.onKeyDown }
                                value={ this.state.smsCode }
                                placeholder='1234'
                                pattern='1111'
                                ref={ 'smsInput' }
                                disabled={ this.props.isSending }
                            />
                            <Label size={ LabelSize.Medium }>Отправить код повторно через 1:00 сек</Label>
                        </FormField>
                    </RowColumn>
                    <RowColumn size={ RowColumnSize.Small }>
                        <Button
                            className={ cn('send-btn') }
                            size={ ButtonSize.Large }
                            onClick={ this.onCodeEntered }
                            view={ ButtonView.Action }
                            disabled={ !this.isValidSmsCode() || this.props.isSending }>
                                { [
                                    'Подтвердить',
                                    <Spin size={ SpinSize.Small } visible={ this.props.isSending }/>
                                ] }
                        </Button>
                    </RowColumn>
                </Row>
                <Row>
                    { this.props.onBackButtonClick &&
                        <RowColumn size={ RowColumnSize.Medium } >
                            <Link
                                size={ LinkSize.Medium }
                                text={ 'Отмена' }
                                onClick={ this.onBackLinkClick }
                            />
                        </RowColumn>
                    }
                </Row>
            </div>
        );
    }

    onCodeEntered = (): void => {
        this.props.onCodeEntered(this.state.smsCode);
    }

    onBackLinkClick = (): void => {
        if (!this.props.isSending) {
            this.props.onBackButtonClick();
        }
    }

    onKeyDown = (e: React.KeyboardEvent): void => {
        if (this.isValidSmsCode() && e.keyCode === 13) {
            this.onCodeEntered();
        }
    }

    isValidSmsCode = (): boolean => {
        return this.state.smsCode.length === config.validSmsCodeLength;
    }

}
