import { spread } from 'arui/src/lib/assign';
import moment from 'moment';

import { TransfersHistoryState } from '../state';
import { Action } from '../redux';
import { ActionType } from '../actions';
import * as actions from '../actions';
import { mocks } from '../mocks/mocks';
import { sortByDate } from '../models/model-helper';

export function transferHistory(state: TransfersHistoryState, action: Action) {

    switch(action.type) {
        case ActionType.ValidateSmsCodeResolve: return onValidateSmsCodeResolve(state, action as any); break;
        case ActionType.LoadTransferHistory: return onLoadTransferHistory(state, action as any); break;
        case ActionType.ShowMoreTransactions: return onShowMoreTransactions(state, action as any); break;
        case ActionType.ChangeTransactionsFilter: return onChangeTransactionsFilter(state, action as any); break;
        case ActionType.RepeatTransaction: return onRepeatTransaction(state, action as any); break;
        default: return state;
    }

    function onValidateSmsCodeResolve(state: TransfersHistoryState, action: actions.ValidateSmsCodeResolve) {
        mocks.transactions.unshift(action.transaction);
        return spread(state, {
            lastTransactionMode: true,
            filter: {
                startDate: action.transaction.dateExecute,
                endDate: action.transaction.dateExecute
            }
        });
    }

    function onLoadTransferHistory(state: TransfersHistoryState, action: actions.LoadTransferHistory) {
        let transactions = sortByDate(mocks.transactions);
        return spread(state, {
            transactions: transactions,
            filter: !state.lastTransactionMode ? {
                startDate: transactions[transactions.length - 1].dateExecute,
                endDate: transactions[0].dateExecute
            } : state.filter
        });
    }

    function onShowMoreTransactions(state: TransfersHistoryState, action: any) {
        return spread(state, {
            lastTransactionMode: false,
            filter: {
                startDate: state.transactions[state.transactions.length - 1].dateExecute,
                endDate: state.transactions[0].dateExecute
            }
        });
    }

    function onChangeTransactionsFilter(state: TransfersHistoryState, action: actions.ChangeTransactionsFilter) {
        return spread(state, {
            lastTransactionMode: false,
            filter: action.filter
        });
    }

    function onRepeatTransaction(state: TransfersHistoryState, action: actions.RepeatTransaction) {
        return spread(state, {
            lastTransactionMode: false
        });
    }

}
