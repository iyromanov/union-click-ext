import { spread } from 'arui/src/lib/assign';
import { cloneDeep } from 'arui/src/lib/cloneDeep';

import { TransferState, initialState } from '../state';
import { Action } from '../redux';
import { ActionType } from '../actions';
import * as actions from '../actions';
import { TransferValidator, ValidationResult } from '../lib/transfer-validator';
import { mocks } from '../mocks/mocks';
import {
    transactionToTransferState,
    recalculateAmountsOnChangeDebitAmount,
    recalculateAmountsOnChangeCreditAmount
} from '../models/model-helper';
import { RecipientType } from '../models/models';

export function transfer(state: TransferState, action: Action) {

    switch(action.type) {
        case ActionType.ChangeDebitAccount: return onChangeDebitAccount(state, action as any); break;
        case ActionType.ChangeRecipientType: return onChangeRecipientType(state, action as any); break;
        case ActionType.ChangeRecipientValue: return onChangeRecipientValue(state, action as any); break;
        case ActionType.ChangeRecipientName: return onChangeRecipientName(state, action as any); break;
        case ActionType.ChangeDebitAmount: return onChangeDebitAmount(state, action as any); break;
        case ActionType.ChangeCreditAmount: return onChangeCreditAmount(state, action as any); break;
        case ActionType.ValidateTransfer: return onValidateTransfer(state, action as any); break;
        case ActionType.EnterSmsCode: return onEnterSmsCode(state, action as any); break;
        case ActionType.ChangeLocation: return onChangeLocation(state, action as any); break;
        case ActionType.UnlockForm: return onUlockForm(state, action as any); break;
        case ActionType.ValidateSmsCode: return onValidateSmsCode(state, action as any); break;
        case ActionType.ValidateSmsCodeResolve: return onValidateSmsCodeResolve(state, action as any); break;
        case ActionType.RepeatTransaction: return onRepeatTransaction(state, action as any);
        default: return state;
    }

    function onChangeDebitAccount(state: TransferState, action: actions.ChangeDebitAccount) {
        let newState = spread(state, {
            debitAccount: state.debitAccounts.filter((account) => {
                return account.number === action.accountNumber;
            })[0]
        });
        let debitAmountResult = TransferValidator.validateDebitAmountFromState(newState);
        return _recalculateIsValid(!!newState.debitAmount ?
            spread(recalculateAmountsOnChangeDebitAmount(newState.debitAmount.value, newState), {
                debitAmountFieldError: debitAmountResult.message,
                isDebitAmountValid: debitAmountResult.ok
            }) : newState);
    }

    function onChangeRecipientType(state: TransferState, action: actions.ChangeRecipientType) {
        //TODO: for process simplification
        return spread(initialState.transfer, {
            recipient: spread(initialState.transfer.recipient, {
                type: action.recipientType
            })
        });
        // return spread(state, {
        //     recipient: spread(state.recipient, {
        //         type: action.recipientType
        //     })
        // });
    }

    function onChangeRecipientValue(state: TransferState, action: actions.ChangeRecipientValue) {
        let newState = _calcualteRecipient(state, action);
        return _recalculateIsValid(newState);
    }

    function onChangeRecipientName(state: TransferState, action: actions.ChangeRecipientName) {
        //TODO: refactor var names
        let result = TransferValidator.validateRecipientName(action.name);
        let recipientFieldError = state.recipient.userValues[state.recipient.type].error;
        let newState = spread(state, {
            recipient: spread(state.recipient, {
                name: action.name,
                account: !recipientFieldError && result.ok ? mocks.recipientAccount : null,
                type: state.recipient.type
            }),
            recipientNameFieldError: result.message,
            isRecipientValid: !recipientFieldError && result.ok
        });
        return _recalculateIsValid(newState);
    }

    function onChangeDebitAmount(state: TransferState, action: actions.ChangeDebitAmount) {
        let result: ValidationResult = TransferValidator.validateDebitAmount(state, action.value);
        let newState = spread(recalculateAmountsOnChangeDebitAmount(action.value, state), {
            debitAmountFieldError: result.message,
            isDebitAmountValid: result.ok
        });
        return _recalculateIsValid(newState);
    }

    function onChangeCreditAmount(state: TransferState, action: actions.ChangeCreditAmount) {
        let result: ValidationResult = TransferValidator.validateCreditAmount(state, action.value);
        let newState = spread(state, {
            creditAmount: {
                value: action.value,
                currency: state.recipient.account.currency
            },
            debitAmount: {
                value: (action.value / mocks.exchangeRate) * state.debitAccount.currency.minorNumber,
                currency: state.debitAccount.currency
            },
            feeAmount: {
                value: ((action.value / mocks.exchangeRate) * state.debitAccount.currency.minorNumber) * mocks.feeRate,
                currency: state.debitAccount.currency
            },
            debitAmountFieldError: result.message,
            isDebitAmountValid: result.ok
        });
        return _recalculateIsValid(newState);
    }

    function onValidateTransfer(state: TransferState, action: any) {
        let newState = TransferValidator.validateTransferState(state);
        newState = _recalculateIsValid(newState);
        newState.isReadOnly = newState.isValid;
        return newState;
    }

    function onUlockForm(state: TransferState, action: any) {
        return spread(state, {
            isReadOnly: false
        });
    }

    function onEnterSmsCode(state: TransferState, action: actions.EnterSmsCode) {
        return state;
    }

    function onChangeLocation(state: TransferState, action: any) {
        return spread(state, {
            location: action.value
        });
    }

    function onValidateSmsCode(state: TransferState, action: any) {
        return spread(state, {
            isSmsCodeSending: true
        });
    }

    function onValidateSmsCodeResolve(state: TransferState, action: any) {
        return spread(initialState.transfer, {
            isSmsCodeSending: false,
            location: '/history'
        });
    }

    function onRepeatTransaction(state: TransferState, action: any) {
        return spread(transactionToTransferState(action.transaction, state), {
            location: '/'
        });
    }

    function _recalculateIsValid(state: TransferState): TransferState {
        return spread(state, {
            isValid: state.isRecipientValid && state.isDebitAccountValid
                && state.isDebitAmountValid
        });
    }

    function _calcualteRecipient(state: TransferState, action: actions.ChangeRecipientValue): TransferState {
        let result: ValidationResult = TransferValidator.validateRecipientValue(action.value, state.recipient.type);
        let isPhoneNumber = state.recipient.type === RecipientType.PhoneNumber;
        let hasAccount = _calculateHasAccount(result.ok, state);
        let updUserValues = cloneDeep(state.recipient.userValues);
        updUserValues[state.recipient.type] = { value: action.value, error: result.message };
        return spread(state, {
            recipient: spread(state.recipient, {
                account: hasAccount ? spread(mocks.recipientAccount,
                    isPhoneNumber ? {} : {
                        number: action.value
                    }) : null,
                userValues: updUserValues
            }),
            recipientFieldError: result.message,
            isRecipientValid: result.ok && hasAccount
        });
    }

    function _calculateHasAccount(isValidRecipientValue: boolean, state: TransferState): boolean {
        let isPhoneNumber = state.recipient.type === RecipientType.PhoneNumber;
        if (isPhoneNumber) {
            return isValidRecipientValue && !!state.recipient.name;
        } else {
            return isValidRecipientValue;
        }
    }

}
