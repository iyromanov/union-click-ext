import { State, Action } from '../redux';
import { transfer } from './transfer';
import { transferHistory } from './transfer-history';
import { ActionType } from '../actions';


export default function app(state: State, action: Action): State {
    return {
        transfer: transfer(state.transfer, action),
        transfersHistory: transferHistory(state.transfersHistory, action)
    }
}
