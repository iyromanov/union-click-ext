var webpack = require('webpack');
var path = require("path");
var fs = require("fs");

var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function (x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function (mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

var isProduction = process.env.NODE_ENV == 'production';
var isTest = process.env.NODE_ENV == 'test';
var doTypeCheck = true;

var typeLib = path.join(__dirname, 'src', 'defines.d.ts');

var externals = [
    '&externals[]=' + path.join(process.cwd(), 'node_modules', 'arui', 'src', 'defines.d.ts'),
    '&externals[]=' + path.join(process.cwd(), 'node_modules', 'arui-nyc', 'src', 'defines.d.ts'),
    '&externals[]=' + path.join(process.cwd(), 'src', 'defines.d.ts')
].join('');

var ATL_OPTIONS = [
    '&target=es6',
    '&jsx=react',
    '&+useCache',
    '&+useBabel',
    '&+experimentalDecorators',
    externals
].join('');

if (!isProduction && !isTest) {
    ATL_OPTIONS += '&+forkChecker';
}

if (!doTypeCheck) {
    ATL_OPTIONS += '&-doTypeCheck';
}

module.exports = {

    entry: {
        server: './src/server/index.node.js'
    },

    target: "node",
    externals: nodeModules,

    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: '/assets/',
        filename: '[name].js',
        libraryTarget: "commonjs2"
    },

    resolve: {
        extensions: ['', '.ts', '.tsx', '.js', '.styl'],
        root: [
            path.join(__dirname, 'node_modules'),
            path.join(__dirname, 'bower_components'),
            path.join(__dirname, 'node_modules/arui/bower_components'),
            path.join(__dirname, 'node_modules/arui/node_modules'),
            path.join(__dirname, 'node_modules/arui-nyc/bower_components'),
            path.join(__dirname, 'node_modules/arui-nyc/node_modules'),
            path.join(__dirname, 'node_modules/alfa-components/node_modules')
        ]
    },

    // Source maps support (or 'inline-source-map' also works)
    devtool: 'inline-source-map',

    module: {
        loaders: [
            {
                test: /(\.ts|\.tsx)$/,
                loaders: ["awesome-typescript-loader?" + ATL_OPTIONS]
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader'
            },
            {
                test: /\.json$/,
                loader: "json-loader"
            },
            {
                test: /\.handlebars$/,
                loader: "handlebars-loader"
            }
        ]
    },

    plugins: [
        new webpack.NormalModuleReplacementPlugin(/(\.styl|\.css)$/, 'node-noop'),
        new webpack.NormalModuleReplacementPlugin(/tether/, 'node-noop'),
        new webpack.IgnorePlugin(/\.(styl)$/),
        new webpack.DefinePlugin({
            ENV: {
                debug: true
            }
        })
    ]
};
