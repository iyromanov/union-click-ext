var fs = require('fs');
var _ = require('lodash');
var archiver = require('archiver');

function packApp() {
    console.log('Create tar archive with deps...');
    var pkg = require('./package.json');

    var output = fs.createWriteStream('./.build/app.tar.gz');
    output.on('close', function() {
        console.log('total ' + Math.round(archive.pointer() / 1024 / 1024) + ' MB');
        console.log('archiver has been finalized and the output file descriptor has closed.');
    });

    var archive = archiver.create('tar', {
        gzip: true,
        gzipOptions: {
            level: 1
        }
    });

    archive.pipe(output);
    archive.on('error', function(err) {
        throw err;
    });

    _.forEach(pkg.dependencies, function(version, dep) {
        console.log(dep, version);
        archive.directory('./node_modules/' + dep);
    });

    archive.directory('./dist')
    archive.file('./package.json')

    archive.finalize();
}

packApp();
