var webpack = require('webpack');
var path = require("path");
var fs = require("fs");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var _ = require('./node_modules/arui/node_modules/lodash-node');

module.exports = function(options) {
    var isTest = options.isTest;
    var isProduction = options.isProduction;
    var isStandalone = options.isStandalone;
    var doTypeCheck = options.doTypeCheck;

    var externals = [
        '&externals[]=' + path.join(process.cwd(), 'node_modules', 'arui', 'src', 'defines.d.ts'),
        '&externals[]=' + path.join(process.cwd(), 'node_modules', 'arui-nyc', 'src', 'defines.d.ts'),
    ].join('');

    var ATL_OPTIONS = [
        '&target=es6',
        '&jsx=react',
        '&+useCache',
        '&+useBabel',
        '&+experimentalDecorators',
        externals
    ].join('');

    if (!isProduction && !isTest && !isStandalone) {
        ATL_OPTIONS += '&+forkChecker';
    }

    if (!doTypeCheck) {
        ATL_OPTIONS += '&-doTypeCheck';
    }

    var config = {
      cache: true,

      devServer: {
        contentBase: "./dist"
      },

      resolveLoader: { fallback: __dirname + "/node_modules" },

      resolve: {
        root: [
            path.join(__dirname, 'node_modules'),
            path.join(__dirname, 'bower_components'),
            path.join(__dirname, 'node_modules/arui/bower_components'),
            path.join(__dirname, 'node_modules/arui/node_modules'),
            path.join(__dirname, 'node_modules/arui-nyc/bower_components'),
            path.join(__dirname, 'node_modules/arui-nyc/node_modules'),
            path.join(__dirname, 'node_modules/alfa-components/node_modules')
        ],
        extensions: ['', '.ts', '.tsx', '.webpack.js', '.web.js', '.js', '.styl'],
        alias: {
          "lodash": "lodash-node/modern"
        }
      },

      node: {
        fs: "empty"
      },

      // Source maps support (or 'inline-source-map' also works)
      devtool: 'source-map',

      module: {
        loaders: [
          isProduction
            ? {
                test: /(\.ts|\.tsx)$/,
                loader: "awesome-typescript-loader?" + ATL_OPTIONS
              }
            : {
                test: /(\.ts|\.tsx)$/,
                loaders: ["react-hot", "awesome-typescript-loader?" + ATL_OPTIONS]
              },
          {
            test: /\.json?$/,
            loader: "json-loader"
          }
        ]
      },

      plugins: [
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru|en-gb/),
        new webpack.ProvidePlugin({
            React: "react"
        }),
          function() {
              this.plugin("done", function (stats) {
                  fs.writeFileSync(
                      path.join(__dirname, "hash.json"),
                      JSON.stringify({ hash: stats.hash } )
                  );
              });
          }
      ]
    };

    if (!isProduction) {
        config.plugins = config.plugins.concat([
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoErrorsPlugin(),
            new webpack.DefinePlugin({
                DEBUG: true
            })
        ])
    } else {
        config.plugins = config.plugins.concat([
            new webpack.DefinePlugin({
                DEBUG: false
            })
        ])
    }

    if (!isTest) {
        config.module.loaders = config.module.loaders.concat([
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
            { test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
            {
              test: /\.(jpe?g|png|gif|svg)$/i,
              loaders: [
                'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
              ]
            }
        ]);

        if (!isProduction) {
            config.module.loaders = config.module.loaders.concat([
                {
                  test: /\.css$/,
                  loader: "style-loader!css-loader!postcss-loader"
                },
                {
                  test: /\.styl$/,
                  loader: 'style-loader!css-loader!stylus-loader'
                }
            ]);
        } else {
            config.module.loaders = config.module.loaders.concat([
                {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader')
                },
                {
                    test: /\.styl$/,
                    loader: ExtractTextPlugin.extract("style-loader", "css-loader!stylus-loader")
                }
            ]);
        }

        config.plugins = config.plugins.concat([
            new webpack.optimize.CommonsChunkPlugin("vendor.bundle.[hash].js", ["app", "vendor"])
        ]);

        config.plugins.push(new ExtractTextPlugin("[name].[hash].css", {
            allChunks: true,
            disabled: !isProduction
        }));

        _.assign(config, {
            entry: {
              app: './src/entries/index.tsx',
              vendor: [
                "react",
                "react-dom"
              ]
            },

            output:{
              path: path.join(__dirname, 'dist', 'assets'),
              publicPath: isProduction ? undefined : '/assets/',
              filename: '[name].[hash].js'
            }
        });
        config.postcss = [
            require('postcss-nested'),
            require('autoprefixer'),
            require('lost'),
            require('postcss-custom-properties')({
            })
        ];
    } else {
        config.plugins = config.plugins.concat([
            new webpack.NormalModuleReplacementPlugin(/(\.css|\.png|\.svg|\.styl)$/, 'node-noop')
        ])
    }

    return config;
}
